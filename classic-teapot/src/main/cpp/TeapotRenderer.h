/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//--------------------------------------------------------------------------------
// Teapot Renderer.h
// Renderer for teapots
//--------------------------------------------------------------------------------
#ifndef _TEAPOTRENDERER_H
#define _TEAPOTRENDERER_H

//--------------------------------------------------------------------------------
// Include files
//--------------------------------------------------------------------------------
#include <jni.h>
#include <errno.h>

#include <vector>

#include <EGL/egl.h>
#include <GLES/gl.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>
#include <android/native_window_jni.h>

#define XR_USE_PLATFORM_ANDROID
#define XR_USE_GRAPHICS_API_OPENGL_ES
#include <openxr/openxr_platform.h>
#include <openxr/openxr_structs.hpp>
#include <openxr/openxr_handles.hpp>

#define CLASS_NAME "android/app/NativeActivity"
#define APPLICATION_CLASS_NAME "com/sample/teapot/TeapotApplication"

#include "NDKHelper.h"

#define BUFFER_OFFSET(i) ((char*)NULL + (i))

struct SwapchainData
{
    xr::UniqueDynamicSwapchain swapchain;
    std::vector<xr::SwapchainImageOpenGLESKHR> images;
    std::vector<GLuint> framebuffers;
};

struct TEAPOT_VERTEX {
  float pos[3];
  float normal[3];
};

enum SHADER_ATTRIBUTES {
  ATTRIB_VERTEX,
  ATTRIB_NORMAL,
  ATTRIB_UV,
};

struct SHADER_PARAMS {
  GLuint program_;
  GLuint light0_;
  GLuint material_diffuse_;
  GLuint material_ambient_;
  GLuint material_specular_;

  GLuint matrix_projection_;
  GLuint matrix_view_;
};

struct TEAPOT_MATERIALS {
  float diffuse_color[3];
  float specular_color[4];
  float ambient_color[3];
};

class TeapotRenderer {
  int32_t num_indices_;
  int32_t num_vertices_;
  GLuint ibo_;
  GLuint vbo_;

  SHADER_PARAMS shader_param_;
  bool LoadShaders(SHADER_PARAMS* params, const char* strVsh,
                   const char* strFsh);

  ndk_helper::Mat4 mat_projection_[2];
  ndk_helper::Mat4 mat_view_[2];
  ndk_helper::Mat4 mat_model_;

  ndk_helper::TapCamera* camera_;

 public:
  TeapotRenderer();
  virtual ~TeapotRenderer();
  void Init();
  void Render(int viewIndex);
  void Update(std::vector<xr::View> const& views);
  bool Bind(ndk_helper::TapCamera* camera);
  void Unload();
  void UpdateViewport();
};

#endif
